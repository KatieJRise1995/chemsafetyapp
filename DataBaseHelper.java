package com.chemicalsafety.xxfal_000.chemsafetyapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
public class DataBaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "chemicals.db";
    private static final String TABLE_OSHA = "osha";
    public static final String COLUMN_COMPOUND = "compound";
    public static final String COLUMN_H = "h";
    public static final String COLUMN_F = "f";
    public static final String COLUMN_R = "r";
    public static final String COLUMN_SN = "sn";

    public DataBaseHelper(Context context, String name,
                          SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_OSHA_TABLE = "CREATE TABLE " +
                TABLE_OSHA + "("
                + COLUMN_COMPOUND + " TEXT," + COLUMN_H
                + " TEXT," + COLUMN_F + " TEXT," + COLUMN_R + " TEXT," + COLUMN_SN + " TEXT" + ")";
        db.execSQL(CREATE_OSHA_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion,
                          int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_OSHA);
        onCreate(db);
    }
    public void addCompound(Compound compound) {

        ContentValues values = new ContentValues();
        values.put(COLUMN_COMPOUND, compound.getCompound());
        values.put(COLUMN_H, compound.getH());
        values.put(COLUMN_F, compound.getF());
        values.put(COLUMN_R, compound.getR());
        values.put(COLUMN_SN, compound.getSN());

        SQLiteDatabase db = this.getWritableDatabase();

        db.insert(TABLE_OSHA, null, values);
        db.close();
    }
    public Compound findCompound(String compoundname) {
        String query = "Select * FROM " + TABLE_OSHA + " WHERE " + COLUMN_COMPOUND + "= '" + compoundname + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        Compound compound = new Compound();
        if (cursor.moveToFirst()) {
            cursor.moveToFirst();
            compound.setH(cursor.getString(1));
            compound.setF(cursor.getString(2));
            compound.setR(cursor.getString(3));
            compound.setSN(cursor.getString(4));
            // cursor.moveToNext();
            cursor.close();
        } else {
            compound = null;
        }
        db.close();
        return compound;
    }
    public boolean deleteCompound(String compoundname) {

        boolean result = false;

        String query = "Select * FROM " + TABLE_OSHA + " WHERE " + COLUMN_COMPOUND + " =  \"" + compoundname + "\"";

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(query, null);

        Compound compound = new Compound();

        if (cursor.moveToFirst()) {
            compound.setCompound(cursor.getString(0));
            db.delete(TABLE_OSHA, COLUMN_COMPOUND + " = ?",
                    new String[] { String.valueOf(compound.getCompound()) });
            cursor.close();
            result = true;
        }
        db.close();
        return result;
    }
}