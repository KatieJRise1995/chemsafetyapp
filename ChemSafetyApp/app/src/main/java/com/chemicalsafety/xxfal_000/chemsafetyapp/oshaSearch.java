package com.chemicalsafety.xxfal_000.chemsafetyapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class oshaSearch extends AppCompatActivity {
    TextView txtH;
    TextView txtR;
    TextView txtF;
    TextView txtSN;
    EditText boxH;
    EditText boxR;
    EditText boxF;
    EditText boxSN;
    EditText txtCompound;
    Button btnSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_osha_search);
        txtF = (TextView) findViewById(R.id.txtF);
        txtR = (TextView) findViewById(R.id.txtR);
        txtH = (TextView) findViewById(R.id.txtH);
        txtSN = (TextView) findViewById(R.id.txtSN);
        txtCompound = (EditText) findViewById(R.id.searchCompound);
        boxSN = (EditText) findViewById(R.id.boxSN);
        boxF = (EditText) findViewById(R.id.boxF);
        boxR = (EditText) findViewById(R.id.boxR);
        boxH = (EditText) findViewById(R.id.boxH);
        btnSearch = (Button) findViewById(R.id.btnSearch);
    }
    public void newCompound (View view) {
        DataBaseHelper dbHandler = new DataBaseHelper(this, null, null, 1);

        Compound compound =
                new Compound(txtCompound.getText().toString(),
                        boxH.getText().toString(),
                        boxF.getText().toString(),
                        boxR.getText().toString(),
                        boxSN.getText().toString());

        dbHandler.addCompound(compound);
        txtCompound.setText("");
        boxH.setText("");
        boxF.setText("");
        boxR.setText("");
        boxSN.setText("");
    }
    public void lookupCompound (View view) {
        DataBaseHelper dbHandler = new DataBaseHelper(this, null, null, 1);

        Compound compound =
                dbHandler.findCompound(txtCompound.getText().toString());

        if (compound != null) {
            //txtCompound.setText(String.valueOf(compound.getCompound()));
            txtH.setText(String.valueOf(compound.getH()));
            txtF.setText(String.valueOf(compound.getF()));
            txtR.setText(String.valueOf(compound.getR()));
            txtSN.setText(String.valueOf(compound.getSN()));
        } else {
            Toast.makeText(oshaSearch.this, "No Match Found", Toast.LENGTH_SHORT).show();
        }
    }
    public void removeCompound (View view) {
        DataBaseHelper dbHandler = new DataBaseHelper(this, null,
                null, 1);

        boolean result = dbHandler.deleteCompound(
                txtCompound.getText().toString());

        if (result)
        {
            Toast.makeText(oshaSearch.this, "You deleted the compound", Toast.LENGTH_SHORT).show();
        }
        else
            Toast.makeText(oshaSearch.this, "No Match Found", Toast.LENGTH_SHORT).show();
    }
}