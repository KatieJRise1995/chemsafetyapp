package com.chemicalsafety.xxfal_000.chemsafetyapp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button button;
    Button btn2;
    Button btn3;
    Button btn4;
    Button btn5;
    Button btn6;
    Button btn7;
    Button btn8;
    Button btn9;
    Button btn11;
    Button btn12;
    Button btnDwnLd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main);
        button = (Button) findViewById(R.id.button);
        btn2 = (Button) findViewById(R.id.button2);
        btn3 = (Button) findViewById(R.id.button3);
        btn4 = (Button) findViewById(R.id.button4);
        btn5 = (Button) findViewById(R.id.button5);
        btn6 = (Button) findViewById(R.id.button6);
        btn7 = (Button) findViewById(R.id.button7);
        btn8 = (Button) findViewById(R.id.button8);
        btn9 = (Button) findViewById(R.id.button9);
        btn11 = (Button) findViewById(R.id.button12);
        btn12 = (Button) findViewById(R.id.button13);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, oshaSearch.class));
            }
        });
        btnDwnLd = (Button) findViewById(R.id.button10);
        btnDwnLd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://drive.google.com/file/d/0B-dKVh-a6WJ5QlZvYWwwNm5sV0d1Ri1Yb3J0NWY1SzBRTHB3/view?usp=sharing")));
            }
        });
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://drive.google.com/file/d/0B-dKVh-a6WJ5TnpJTlJyM2pOelJGV1BIWjZWdmpLc1ZGdVpj/view?usp=sharing")));
            }
        });
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://drive.google.com/open?id=0B-dKVh-a6WJ5U09pSEtwLVMyWm90THlHUFBCc1E4Z3J2cFE0")));
            }
        });
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://drive.google.com/open?id=0B-dKVh-a6WJ5S0lmeXhXVzNjYy1HOEtmRUZMM3l3ODhtRzRr")));
            }
        });
        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://drive.google.com/open?id=0B-dKVh-a6WJ5eW1IRmdLVTMtNnNnWXlGak5rYzZLYmJQV0pB")));
            }
        });
        btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://drive.google.com/open?id=0B-dKVh-a6WJ5VWFRdHZ1V2tBdDhxNDNLZUtqVnlrQllmT1VF")));
            }
        });
        btn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://drive.google.com/open?id=0B-dKVh-a6WJ5SGdYRWhsaUYyaG1zNzFWelRoQm9VcGNXTnFZ")));
            }
        });
        btn8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://drive.google.com/open?id=0B-dKVh-a6WJ5YThGZEZtTkR3anExNm9qLU5kLXAtWEU4X2xr")));
            }
        });
        btn9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://drive.google.com/open?id=0B-dKVh-a6WJ5ZERQbktya0V5dWp5dmIzcEFSX1RTYW80R2Z3")));
            }
        });
        btn11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://drive.google.com/open?id=0B-dKVh-a6WJ5Q1c2TktqUVhjSHBua0hNdlpwTUJrVWFoaVBZ")));
            }
        });
        btn12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://drive.google.com/open?id=0B-dKVh-a6WJ5VmlJSWRMcFVpY2ZnVmJHSUxzZnpOa2QzU1NV")));
            }
        });
    }
}

