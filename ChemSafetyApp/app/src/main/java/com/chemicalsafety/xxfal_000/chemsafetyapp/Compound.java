package com.chemicalsafety.xxfal_000.chemsafetyapp;

/**
 * Created by xxfal_000 on 3/21/2016.
 */
public class Compound {

    private String  _compound;
    private String _h;
    private String _f;
    private String _r;
    private String _sn;

    public Compound() {

    }

    public Compound(String compound, String h, String f, String r, String sn) {
        this._compound = compound;
        this._h = h;
        this._f = f;
        this._r = r;
        this._sn = sn;
    }

    public Compound(String h, String f, String r, String sn) {
        this._h = h;
        this._f = f;
        this._r = r;
        this._sn = sn;
    }

    public void setCompound(String compound) {
        this._compound = compound;
    }

    public String getCompound() {
        return this._compound;
    }

    public void setH(String h) {
        this._h = h;
    }

    public String getH() {
        return this._h;
    }

    public void setF(String f) {
        this._f = f;
    }

    public String getF() {
        return this._f;
    }
    public void setR(String r) {
        this._r = r;
    }
    public String getR(){
        return this._r;
    }
    public void setSN(String sn){
        this._sn = sn;
    }
    public String getSN(){
        return this._sn;
    }
}