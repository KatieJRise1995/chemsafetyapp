package com.chemicalsafety.xxfal_000.chemsafetyapp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button buttonDB;
    Button btnPF;
    Button btnHood;
    Button btnPEL;
    Button btnTemp;
    Button btnGlove;
    Button btnCheck;
    Button btnGHS;
    Button btnLbl;
    Button btnDwnLd;
    Button btnIno;
    Button btnOrg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main);
	

        buttonDB = (Button) findViewById(R.id.button);
        btnPF = (Button) findViewById(R.id.button2);
        btnHood = (Button) findViewById(R.id.button3);
        btnPEL = (Button) findViewById(R.id.button4);
        btnTemp = (Button) findViewById(R.id.button5);
        btnGlove = (Button) findViewById(R.id.button6);
        btnCheck = (Button) findViewById(R.id.button7);
        btnGHS = (Button) findViewById(R.id.button8);
        btnLbl = (Button) findViewById(R.id.button9);
	btnDwnLd = (Button) findViewById(R.id.button10);
        btnIno = (Button) findViewById(R.id.button12);
        btnOrg = (Button) findViewById(R.id.button13);

	//Leads to db
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, oshaSearch.class));
            }
        });
        
        btnPF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://drive.google.com/file/d/0B-dKVh-a6WJ5TnpJTlJyM2pOelJGV1BIWjZWdmpLc1ZGdVpj/view?usp=sharing")));
            }
        });
        btnHood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://drive.google.com/open?id=0B-dKVh-a6WJ5U09pSEtwLVMyWm90THlHUFBCc1E4Z3J2cFE0")));
            }
        });
        btnPEL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://drive.google.com/open?id=0B-dKVh-a6WJ5S0lmeXhXVzNjYy1HOEtmRUZMM3l3ODhtRzRr")));
            }
        });
        btnTemp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://drive.google.com/open?id=0B-dKVh-a6WJ5eW1IRmdLVTMtNnNnWXlGak5rYzZLYmJQV0pB")));
            }
        });
        btnGlove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://drive.google.com/open?id=0B-dKVh-a6WJ5VWFRdHZ1V2tBdDhxNDNLZUtqVnlrQllmT1VF")));
            }
        });
        btnCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://drive.google.com/open?id=0B-dKVh-a6WJ5SGdYRWhsaUYyaG1zNzFWelRoQm9VcGNXTnFZ")));
            }
        });
        btnGHS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://drive.google.com/open?id=0B-dKVh-a6WJ5YThGZEZtTkR3anExNm9qLU5kLXAtWEU4X2xr")));
            }
        });
        btnLbl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://drive.google.com/open?id=0B-dKVh-a6WJ5ZERQbktya0V5dWp5dmIzcEFSX1RTYW80R2Z3")));
            }
        });
        btnDwnLd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://drive.google.com/file/d/0B-dKVh-a6WJ5QlZvYWwwNm5sV0d1Ri1Yb3J0NWY1SzBRTHB3/view?usp=sharing")));
            }
        });        
	btnIno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://drive.google.com/open?id=0B-dKVh-a6WJ5Q1c2TktqUVhjSHBua0hNdlpwTUJrVWFoaVBZ")));
            }
        });
        btnOrg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://drive.google.com/open?id=0B-dKVh-a6WJ5VmlJSWRMcFVpY2ZnVmJHSUxzZnpOa2QzU1NV")));
            }
        });
    }
}

